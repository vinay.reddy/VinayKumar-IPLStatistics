const matches = require('./model/matches');
const deliveries = require('./model/deliveries');
const players = require('./model/players');
require('./test/connection');
 battingDetails().then(function(battingDetails){
  var bowler;
  bowlingStatistics().then(function(bowlingDetails){
    var player=battingDetails.map(function(batsmen){
       bowler =bowlingDetails.find(function(bowling){
        return bowling.name===batsmen.name&&bowling.season===batsmen.season;
      })
      var obj=batsmen
      if(bowler!==undefined){
      obj['wickets']= bowler.wickets
      obj['economy']= bowler.economy
      bowlingDetails=bowlingDetails.filter(function(bowling){
        return !(bowling.name===bowler.name&&bowling.season===bowler.season);
      })
      }
      return obj;
     })
     players.create(player)
     var bowerDetails=bowlingDetails.map(function(bowler){
       var obj=bowler
           return obj
          })
          players.create(bowerDetails)
    })
  })

function battingDetails(){
return deliveries.aggregate([
  {
    $lookup: {
      from: "matches",
      localField: "matchId",    // field in the orders collection
      foreignField: "matchId",  // field in the items collection
      as: "fromItems"
    }
  },
  {
    $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ["$fromItems", 0] }, "$$ROOT"] } }
  },
  { $project: { fromItems: 0 } },
  {
    $group: {
      _id: { year: '$season', team: '$battingTeam', player: '$batsmanName' },
      runs: { $sum: '$batsmanRuns' }, noOfBallsBatted: { $sum: 1 }
    }
  }
]).then(function (teamDatabase) {
 return deliveries.aggregate([
    {
      $lookup: {
        from: "matches",
        localField: "matchId",    // field in the orders collection
        foreignField: "matchId",  // field in the items collection
        as: "fromItems"
      }
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ["$fromItems", 0] }, "$$ROOT"] } }
    },
    { $project: { fromItems: 0 } },
    {
      $group: {
        _id: { year: '$season', team: '$battingTeam', player: '$batsmanName', matchId: '$matchId' },
        runs: { $sum: '$batsmanRuns' }, noOfBallsBatted: { $sum: 1 }
      }
    }
  ]).then(function (results) {
    return teamDatabase.map(function (data) {
      var noofThirties = 0, noofFifties = 0, avg = 0, noofHundreds = 0, strikeRate = 0, highest = 0;
      results.forEach(function (result) {
        if (data['_id']['team'] === result['_id']['team'] && result['_id']['player'] === data['_id']['player'] && data['_id']['year'] === result['_id']['year']) {
          if (result['runs'] >= 30 && result['runs'] < 50) {
            noofThirties++;
          }
          else if (result['runs'] >= 50 && result['runs'] < 100) {
            noofFifties++;
          }
          else if (result['runs'] >= 100) {
            noofHundreds++;
          }
          if (result['runs'] > highest) {
            highest = result['runs'];
          }
        }
      })
      strikeRate = data['runs'] * 100 / data['noOfBallsBatted'];
      var obj={
          team: data['_id']['team'],
          runs: data['runs'],
          name: data['_id']['player'],
          season: data['_id']['year'],
          numberofthirties: noofThirties,
          numberOfFifties: noofFifties,
          numberofHundereds: noofHundreds,
          strikeRate: strikeRate,
          highest: highest
  }
  return obj;
    })
  }).then(function(result){
    return result;
  })
})
}
function bowlingStatistics(){
return deliveries.aggregate([
  {
    $lookup: {
      from: "matches",
      localField: "matchId",    // field in the orders collection
      foreignField: "matchId",  // field in the items collection
      as: "fromItems"
    }
  },
  {
    $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ["$fromItems", 0] }, "$$ROOT"] } }
  },
  {
    $project: {
      fromItems: 0
    }
  },
  {
    $project: {
      noOfRunsgiven: { $subtract: ["$totalruns", { $add: ["$legByes", "$byes"] }] },
      economy: { $multiply: [{ $subtract: ["$totalruns", { $add: ["$legByes", "$byes"] }] }, 6] },
      season: 1,
      bowlingTeam: 1,
      bowlerName: 1,
      playerDismissed: 1,
      dismissalKind: 1
    }
  },
  {
    $group: {
      _id: { year: '$season', team: '$bowlingTeam', player: '$bowlerName' },
      wickets: { $sum: { $cond: [{ $and: [{ $ne: ["$playerDismissed", ''] }, { $ne: ["dismissalKind", 'run out'] }] }, 1, 0] } },
      noOfRunsgiven: { $sum: '$noOfRunsgiven' },
      economy: { $avg: "$economy" }


    }
  }

]).then(function (teamDatabase) {
  return teamDatabase.map(function (data,index) {
    var obj={
         team: data['_id']['team'],
      wickets: data['wickets'],
      name: data['_id']['player'],
      season: data['_id']['year'],
      economy: data['economy']
    }
    return obj;
  });
}).then(function (result) {
  return result;
})
}



