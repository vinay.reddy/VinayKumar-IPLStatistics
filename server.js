var express = require('express');
const router=require('./routes/api')
var path = require('path');
require('./test/connection');
var app = express();
const port = 5000;
app.use(express.static(path.join(__dirname, './webapp')));
app.use(router);
app.listen(port, function () {
  console.log('Magic happens on port ' + port);
});