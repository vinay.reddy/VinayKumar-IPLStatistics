const mongoose=require('mongoose');
var MongoClient = require('mongodb').MongoClient;
mongoose.connect("mongodb://localhost/config");
mongoose.Promise=global.Promise;
mongoose.connection.once('open',function(){
    console.log("connection is established");
}).on('error',function(error){
    console("connection error:",error);
});
