const mongoose=require('mongoose');
const matches=require('./model/matches');
const deliveries=require('./model/deliveries')
require('./test/connection')
var fs = require('fs');
var deliveriesConverter = require("csvtojson");
deliveriesConverter()
.fromFile('deliveries.csv')
.on('json',(jsonObj)=>{
    deliveries.create({
    matchId:jsonObj['match_id'],
    battingTeam:jsonObj['batting_team'],
    bowlingTeam:jsonObj['bowling_team'],
    batsmanName:jsonObj['batsman'],
    bowlerName:jsonObj['bowler'],
    byes:jsonObj['bye_runs'],
    legByes:jsonObj['legbye_runs'],
    totalruns:jsonObj['total_runs'],
    batsmanRuns:jsonObj['batsman_runs'],
    playerDismissed:jsonObj['player_dismissed'],
    dismissalKind:jsonObj['dismissal_kind']
})
})
.on('done',(error)=>{
    console.log('end')
    var matchesConverter=require('csvtojson');
matchesConverter()
.fromFile('matches.csv')
.on('json',(jsonObj)=>{
matches.create({
    matchId:jsonObj['id'],
    season:jsonObj['season'],
    team1:jsonObj['team1'],
    team2:jsonObj['team2'],
    winner:jsonObj['winner']
})
}) .on('done',(error)=>{
    console.log('end');   
    });
})
