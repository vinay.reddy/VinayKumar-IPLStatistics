$(document).ready(function () {
    fetch('/seasons')
      .then(function (response) {
        return response.json();
      })
      .then(function (myJson) {
        $.each(myJson, function (index, value) {
        $('.menu').append('<a href="#"><li id='+value+' class="seasons"><span>' + "+ " + value + '</span></li></a>')
       });
      });
    
      $(document.body).on('click', '.seasons', function (event) {
        var year=$(this).children(':first-child').text().split(' ')[1];
        var thisVar=this;
        if($(this).children(':first-child').text()==='+ '+year)
        {
          $(this).children(':first-child').text('- '+year)
        }
        else{
          $(this).children(':first-child').text('+ '+year)
        }
          if($(thisVar).children().length===1)
         {
          fetch('/seasons?season='+year)
          .then(function (response) {
            return response.json();
          })
          .then(function (myJson) {
            $(thisVar).append('<ul class="player-teams"></ul>')
      $.each(myJson, function (index, value) {
         var idTeam=value.split(' ').join('-');
        $(thisVar).find('ul').append('<li id='+idTeam+' class="teams"><span>+ ' + value+ '</span></li>')
      })
      })
    }
    $(this).find('ul').toggle();
    })
    $(document.body).on('click', '.teams', function (event) {
      event.stopPropagation();
      var team=$(this)[0].id.split('-').join(' ');
      var year=$(this).parents('.seasons')[0].id;
      thisVar=this;
      if($(this).children(':first-child').text()==='+ '+team)
      {
        $(this).children(':first-child').text('- '+team)
      }
      else{
        $(this).children(':first-child').text('+ '+team)
      }
      
        if($(thisVar).children().length===1)
        {
          fetch('/seasons?season='+year+'&'+'team='+team)
      .then(function (response) {
        return response.json();
      })
      .then(function (myJson) {
          $(thisVar).append('<ul class="player-items"></ul>')
          $.each(myJson, function (index, value) {
              $(thisVar).find('ul').append('<li id='+value+' class="players"><span>' +value+ '</span></li>')
          })
      })
    }
    $(thisVar).find('ul').toggle();
     })
    $(document.body).on('click', '.players', function (event) {
      event.stopPropagation();
      var thisVar=this;
      var year=$(this).parents('.seasons')[0].id;
      var team=$(this).parents('.teams')[0].id.split('-').join(' ');
      var player=$(this).text();
          var name=$(thisVar).text().concat(" "+year).split(' ').join('-');
          if($('#'+name).children().length===0)
          {
            fetch('/seasons?season='+year+'&'+'team='+team+'&'+'player='+player)
            .then(function (response) {
              return response.json();
            })
            .then(function (myJson) {
           $('.details').find('table').remove();
           $('.details').find('div').remove();
           fetch('http://cricapi.com/api/playerFinder?apikey=TQ5Kpg15m3UGmcYBmIxGLnDGIpF2&name='+player)
           .then(function (response) {
            return response.json();
          }).then(function(response){
            if(response['data'][0]===undefined)
            {
              $('.details').append('<div class=playerDiv><img class="playerImg defaultImg" src="consultant2.jpg"></div>')
            }else{
                var pid = response['data'][0]['pid']
          $('.details').append('<div class=playerDiv><img class=playerImg src="http://cricapi.com/playerpic/'+pid+'.jpg"></div>')
            }
          $('.details').append('<table id='+name+' class="player-statistics"></table>')
          $('.player-statistics').append('<tr  class="player-details"><td>Player</td><td>'+player+'</td></tr>')
            $('.player-statistics').append('<tr  class="player-details"><td>Season</td><td>'+year+'</td></tr>')
            $('.player-statistics').append('<tr  class="player-details"><td>Team</td><td>'+team+'</td></tr>')
                 $.each(myJson, function (index, value) {
                var details=Object.keys(value);
                $.each(details, function (index, detail)
                {
                if(detail!=='_id'&&detail!=='__v')
                {
                  $('.player-statistics').append('<tr  class="player-details"><td>'+detail+'</td><td>'+value[detail]+'</td></tr>')
                }
                })
              })
         })
    })
  }
  $('.details').find('div').toggle();
  $('.details').find('table').toggle();
     });
    })