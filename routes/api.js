var express = require('express');
const matches = require('../model/matches');
const deliveries = require('../model/deliveries');
const players = require('../model/players');
const router=express.Router();
router.get('/seasons', function (req, res) {

    if (Object.keys(req.query).length === 0) {
         seasonsData();
    }
    else if (Object.keys(req.query).length === 1) {
          teamsData();
    }
    else if (Object.keys(req.query).length === 2) {
  
      playersData();
    }
    else {
      playersDetails();
    }
  function seasonsData(){
    matches.distinct('season').then(function (result) {
  result.sort(function (a, b) {
    return a - b;
  });
  res.send(result);
})
}
function teamsData()
{
    matches.aggregate(
        [
  
      {
        $project: { season: 1, team: ['$team1', '$team2']}
      },
  
  
      {
        $unwind: "$team"
      },
          
          {
            $group:
              {
                _id: { season: '$season' }, teams: { $addToSet: '$team' }
              }
          }
        ]).then(function (result) {
          result.forEach(function (element) {
            if (req.query['season'] == element['_id']['season']) {
              res.send(element['teams']);
            }
          })
        })
}
function playersData()
{
    players.aggregate(
        [
          {
            $group:
              {
                _id: { team: '$team', year: "$season" }, players: { $addToSet: '$name' }
              }
          }
        ]).then(function (result) {
          result.forEach(function (element) {
            if (req.query['season'] == element['_id']['year'] && req.query['team'] == element['_id']['team']) {
              res.send(element['players']);
            }
          })
        })
}
function playersDetails(){
  players.find({season:req.query['season'],name:req.query['player']}).then(function(result){
    res.send(result)
  })
}
})
  module.exports=router;