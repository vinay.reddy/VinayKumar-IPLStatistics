const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const playersSchema=new Schema({
    team:String,
    wickets:Number,
    runs:Number,
    name:String,
    season:Number,
    numberOfBallsBatted:Number,
    numberOfBallsBowled:Number,
    numberOfFifties:Number,
    numberofHundereds:Number,
    numberofthirties:Number,
    strikeRate:Number,
    economy:Number,
    highest:Number

});
const players=mongoose.model('players',playersSchema);
module.exports=players;