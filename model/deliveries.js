const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const deliveriesSchema=new Schema({
    matchId:Number,
    battingTeam:String,
    bowlingTeam:String,
    batsmanName:String,
    bowlerName:String,
    byes:Number,
    legByes:Number,
    totalruns:Number,
    batsmanRuns:Number,
    playerDismissed:String,
    dismissalKind:String
});
const deliveries=mongoose.model('deliveries',deliveriesSchema);
module.exports=deliveries;