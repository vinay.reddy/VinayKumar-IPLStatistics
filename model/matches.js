const mongoose=require('mongoose')
const Schema=mongoose.Schema;
const matchSchema=new Schema({
    matchId:Number,
    season:Number,
    team1:String,
    team2:String,
    winner:String
    });
    const matches=mongoose.model('matches',matchSchema);
    module.exports=matches;